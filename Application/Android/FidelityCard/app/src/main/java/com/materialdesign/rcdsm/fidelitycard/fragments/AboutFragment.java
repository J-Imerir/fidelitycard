package com.materialdesign.rcdsm.fidelitycard.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.zxing.WriterException;
import com.materialdesign.rcdsm.fidelitycard.MainActivity;
import com.materialdesign.rcdsm.fidelitycard.R;

import Util.QRCodeGenerator;
import Util.User;

/**
 * Created by rcdsm on 18/05/15.
 */
public class AboutFragment extends Fragment {

    View view_a;
    MenuItem menuItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        view_a = inflater.inflate(R.layout.about_user_fragment, container, false);
        ImageView qrcodeReduction = (ImageView)view_a.findViewById(R.id.QRCodeReduction);
        qrcodeReduction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.currentUser.useReduction(getActivity(), new User.OnUserListener() {
                    @Override
                    public void setUserAndAuth(User user) {

                    }

                    @Override
                    public void userIsCreated(boolean success, String message) {

                    }

                    @Override
                    public void callToRefreshList() {
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.container, new ListFragment()).commit();
                    }

                    @Override
                    public void refreshReducDisplay() {
                        menuItem.setTitle(String.valueOf(
                                "Reduction : " + User.currentUser.getReduction() + "€"));
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.container, new ListFragment()).commit();
                    }
                });
            }
        });


        QRCodeGenerator qrCodeGenerator = new QRCodeGenerator(getActivity());
        try {
            qrCodeGenerator.generateQRCode_general(String.valueOf(User.currentUser.getReduction()), qrcodeReduction);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return view_a;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menuItem = menu.findItem(R.id.action_example);
    }
}
