package com.materialdesign.rcdsm.fidelitycard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidquery.AQuery;

import Util.BlurrEffect;
import Util.User;

/**
 * Created by rcdsm on 18/05/15.
 */
public class LoginActivity extends Activity implements User.OnUserListener {
    private Context context;
    private String password;
    private String username;
    private User currentUser;
    private AQuery aq;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        aq = new AQuery(this);
        context = this;
        ImageView image = (ImageView)findViewById(R.id.background);

        final EditText usernameView = (EditText)findViewById(R.id.login);
        final EditText passwordView = (EditText)findViewById(R.id.password);
        /*
            Blurring image view.
         */
        BlurrEffect blur = new BlurrEffect();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.background);
        image.setImageBitmap(blur.blur(this, bitmap));

        Button signIn = (Button)findViewById(R.id.sign_in);
        Button signUp = (Button)findViewById(R.id.sign_up);

        /*
            What happened when the user clicked the button to sign in.
         */

        final LoginActivity loginListener = this;

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = usernameView.getText().toString();
                password = passwordView.getText().toString();

                User.getUserOnServor(username, password, aq, context, loginListener);
            }
        });

        /*
            What happened when the user clicked the button to sign up.
         */
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = usernameView.getText().toString();
                password = passwordView.getText().toString();

                User.signUpUser(username, password, aq, loginListener, context);

            }
        });

    }

    @Override
    public void setUserAndAuth(User user){
        if(user != null) {
            this.currentUser = user;
            User.currentUser = user;
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Intent intent = new Intent(LoginActivity.this, MainActivityLollipop.class);
                startActivity(intent);
            }
            else
            {
                Intent intent = new Intent(LoginActivity.this, MainActivityLollipop.class);
                startActivity(intent);
            }

        }
    }

    @Override
    public void userIsCreated(boolean success, String message){
        if(success) {
            Toast.makeText(this, "You can now login", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void callToRefreshList() {

    }

    @Override
    public void refreshReducDisplay() {

    }
}

