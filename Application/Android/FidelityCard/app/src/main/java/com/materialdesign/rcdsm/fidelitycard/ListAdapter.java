package com.materialdesign.rcdsm.fidelitycard;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.androidquery.AQuery;

import java.util.List;

import Util.BlurrEffect;
import Util.Product;
import Util.User;

/**
 * Created by rcdsm on 19/05/15.
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ProductViewHolder> {

    private List<Product> products;
    private Context context;
    private int lastPosition = -1;

    public ListAdapter(List<Product> contactList, Context context) {
        this.products = contactList;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public void onBindViewHolder(ProductViewHolder contactViewHolder, int i) {
        Product ci = products.get(i);
        contactViewHolder.vNumber.setText(String.valueOf(ci.getNumber()));
        contactViewHolder.vName.setText(ci.getName());
        contactViewHolder.vPrice.setText(String.valueOf(ci.getPrice())+"€");
        contactViewHolder.vSubTotalPrice.setText(String.valueOf(ci.getTotalByProduct())+"€");
        AQuery aq = new AQuery(context);
        String thumbnail = ci.getSrc();
        Bitmap preset = aq.getCachedImage(thumbnail);
        if(ci.getSrc().equals("") || ci.getSrc().equals(null))
        {
            contactViewHolder.progressBar.setVisibility(View.GONE);
            contactViewHolder.imageView.setImageResource(R.drawable.logo_fidelity);
        }
        else if(preset != null)
        {
            contactViewHolder.imageView.setImageBitmap(preset);
        }
        else
        {
            aq.id(contactViewHolder.imageView).progress(contactViewHolder.progressBar).image(ci.getSrc(), false, false, 0, 0, preset, AQuery.FADE_IN);
        }
        setAnimation(contactViewHolder.container, i);
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_article, viewGroup, false);

        return new ProductViewHolder(itemView);
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        protected TextView vName;
        protected TextView vPrice;
        protected TextView vNumber;
        protected TextView vSubTotalPrice;
        protected ImageView imageView;
        protected ProgressBar progressBar;
        protected View container;

        public ProductViewHolder(View v) {
            super(v);
            vName = (TextView) v.findViewById(R.id.txtName);
            vPrice = (TextView) v.findViewById(R.id.txtPrice);
            imageView = (ImageView)v.findViewById(R.id.logo_cardview);
            vNumber = (TextView) v.findViewById(R.id.txtNumber);
            vSubTotalPrice = (TextView) v.findViewById(R.id.txtSubTotalPrice);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
            container = (CardView)v.findViewById(R.id.card_view);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
