package com.materialdesign.rcdsm.fidelitycard.fragments;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.materialdesign.rcdsm.fidelitycard.ListAdapter;
import com.materialdesign.rcdsm.fidelitycard.MainActivity;
import com.materialdesign.rcdsm.fidelitycard.R;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

import java.util.ArrayList;
import java.util.List;

import Util.BlurrEffect;
import Util.Product;
import Util.User;

/**
 * Created by rcdsm on 18/05/15.
 */
public class ListFragment extends Fragment{

    View view_a;
    RecyclerView recycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view_a = inflater.inflate(R.layout.list_fragment, container, false);
        TextView tv = (TextView)view_a.findViewById(R.id.total);
        TextView rv = (TextView)view_a.findViewById(R.id.reduc);
        TextView orv = (TextView)view_a.findViewById(R.id.oldReduc);
        tv.setText(String.valueOf(User.currentUser.CalculateTotalPrice()+"€"));
        rv.setText(String.valueOf(User.currentUser.getReduction()+"€"));
        orv.setText(String.valueOf(User.currentUser.getOldReduction()+"€"));
        recycler = (RecyclerView)view_a.findViewById(R.id.cardList);
        recycler.setHasFixedSize(true);
        ImageView image = (ImageView) view_a.findViewById(R.id.bottomImg);
        BlurrEffect blur = new BlurrEffect();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.background);
        image.setImageBitmap(blur.blur(getActivity(), bitmap));
        image.getLayoutParams().width = ActionBar.LayoutParams.MATCH_PARENT;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(layoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());

        List<Product> products = User.currentUser.getProducts();

        ListAdapter listAdapter = new ListAdapter(products, getActivity());
        recycler.setAdapter(listAdapter);

        return view_a;
    }
}
