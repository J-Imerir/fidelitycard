package Util;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.materialdesign.rcdsm.fidelitycard.LoginActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rcdsm on 18/05/15.
 */
public class User {

    public static User currentUser;

    public User(){}

    public User(String pseudo, String pwd, int id, List<Product> products, double reduction, double oldReduction)
    {
        this.setPseudo(pseudo);
        this.setId(id);
        this.setProducts(products);
        this.setPwd(pwd);
        this.setReduction(reduction);
        this.setOldReduction(oldReduction);
    }

    private String pseudo;
    private String pwd;
    private int id;
    private List<Product> Products;
    private double reduction;
    private double oldReduction;

    public double getOldReduction() {
        return MathHelper.round(oldReduction, 2);
    }

    public void setOldReduction(double oldReduction) {
        this.oldReduction = oldReduction;
    }

    public double getReduction() {
        return MathHelper.round(reduction,2);
    }

    public void setReduction(double reduction) {
        this.reduction = reduction;
    }

    public String getPseudo() {

        return pseudo;
    }

    public void setPseudo(String pseudo) {

        this.pseudo = pseudo;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Product> getProducts() {
        return Products;
    }

    public void setProducts(List<Product> products) {
        Products = products;
    }

    public void useReduction(Context context, final OnUserListener listener) {
        AQuery aq = new AQuery(context);
        String url = "http://www.pierre-mar.net/CarteFidelite/Webservice/index.php?useReduc=true&userId=" + User.currentUser.getId();
        final Context currentContext = context;

        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setIndeterminate(false);
        dialog.setCancelable(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Using ...");

        aq.progress(dialog).ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject response, AjaxStatus status) {
                try {
                    String info = response.getString("info");
                    Toast.makeText(currentContext, info, Toast.LENGTH_SHORT).show();
                    if(info.equals("success")){
                        User.currentUser.setOldReduction(User.currentUser.getOldReduction() + User.currentUser.getReduction());
                        User.currentUser.setReduction(0);
                        listener.refreshReducDisplay();
                    }
                }catch(Exception e){
                    Log.d("jsonException", e.getMessage());
                }
            }
        });
    }

    public void addProductToList(String reference, Context context, final OnUserListener listener) {
        AQuery aq = new AQuery(context);
        String url = "http://www.pierre-mar.net/CarteFidelite/Webservice/index.php?addProduct=true&userId=" + User.currentUser.getId() + "&productRef=" + reference;
        final Context currentContext = context;

        /*
            Create Progress Dialog.
         */
        ProgressDialog dialog = new ProgressDialog(context);

        dialog.setIndeterminate(false);
        dialog.setCancelable(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Adding Product...");

        aq.progress(dialog).ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject response, AjaxStatus status) {
                try {
                    String info = response.getString("info");
                    if(info.equals("success")) {
                        double reduc = response.getDouble("reduction");
                        double oldReduc = response.getDouble("oldReduction");
                        JSONArray products = response.getJSONArray("products");
                        List<Product> productList = new ArrayList();
                        for (int i=0; i<products.length(); i++) {
                            JSONObject product = products.getJSONObject(i);
                            int id_product = product.getInt("id");
                            String reference_product = product.getString("reference");
                            String name_product = product.getString("name");
                            double price_product = product.getDouble("price");
                            int number_product = product.getInt("number");
                            String src_product = product.getString("src");
                            productList.add(new Product(id_product, reference_product, name_product, price_product, number_product, src_product));
                        }
                        User.currentUser.setProducts(productList);
                        User.currentUser.setReduction(reduc);
                        User.currentUser.setOldReduction(oldReduc);
                        listener.callToRefreshList();
                    }
                    else{
                        Toast.makeText(currentContext, info, Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){
                    Log.d("jsonException", e.getMessage());
                }
            }
        });
    }

    public static void signUpUser(String username, final String password, AQuery aq, final OnUserListener listener, Context context){
        String url = "http://www.pierre-mar.net/CarteFidelite/Webservice/index.php?set=true&username=" + username + "&inputPassword=" + password;

         /*
            Create Progress Dialog.
         */
        ProgressDialog dialog = new ProgressDialog(context);



        dialog.setIndeterminate(false);
        dialog.setCancelable(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Signing up...");

        aq.progress(dialog).ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject response, AjaxStatus status) {
                try {
                    String info = response.getString("info");
                    if(info.equals("success")) {

                        listener.userIsCreated(true, "");

                    }
                    else{
                        listener.userIsCreated(false, info);
                    }
                }catch(Exception e){
                    Log.d("jsonException", e.getMessage());
                }
            }
        });
    }

    public static void getUserOnServor(String username, final String password, AQuery aq, Context context, final OnUserListener listener){
        String url = "http://www.pierre-mar.net/CarteFidelite/Webservice/index.php?get=true&username=" + username + "&inputPassword=" + password;
        final Context currentContext = context;

         /*
            Create Progress Dialog.
         */
        ProgressDialog dialog = new ProgressDialog(context);

        dialog.setIndeterminate(false);
        dialog.setCancelable(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Signing in...");

        aq.progress(dialog).ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject response, AjaxStatus status) {
                try {
                    String info = response.getString("info");
                    if(info.equals("success")) {
                        int id = response.getInt("id");
                        String username = response.getString("username");
                        double reduction = response.getDouble("reduction");
                        double oldReduction = response.getDouble("oldReduction");
                        JSONArray products = response.getJSONArray("products");
                        List<Product> productList = new ArrayList();
                        for (int i=0; i<products.length(); i++) {
                            JSONObject product = products.getJSONObject(i);
                            int id_product = product.getInt("id");
                            String reference_product = product.getString("reference");
                            String name_product = product.getString("name");
                            double price_product = product.getDouble("price");
                            int number_product = product.getInt("number");
                            String src_product = product.getString("src");
                            productList.add(new Product(id_product, reference_product, name_product, price_product, number_product, src_product));
                        }

                        listener.setUserAndAuth(new User(username, password, id, productList, reduction, oldReduction));

                    }
                    else{
                        Toast.makeText(currentContext, info, Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){
                    Log.d("jsonException", e.getMessage());
                }
            }
        });

    }

    public interface OnUserListener {
        public void setUserAndAuth(User user);
        public void userIsCreated(boolean success, String message);
        public void callToRefreshList();
        public void refreshReducDisplay();
    }


    public int CalculateTotalPrice()
    {
        int total = 0;
        for(Product p : User.currentUser.getProducts())
        {
            total += (p.getNumber() * p.getPrice());
        }
        return total;
    }
}
