package Util;

/**
 * Created by rcdsm on 18/05/15.
 */
public class Product {

    public Product(){}

    public Product(int id, String reference, String name, double price, int number, String src)
    {
        this.setId(id);
        this.setName(name);
        this.setReference(reference);
        this.setPrice(price);
        this.setNumber(number);
        this.setSrc(src);
    }

    private int id;
    private String Reference;
    private String Name;
    private double Price;
    private int Number;
    private String src;

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        Reference = reference;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }

    public double getTotalByProduct()
    {
        return MathHelper.round((this.getNumber() * this.getPrice()),2);
    }
}
