package Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rcdsm on 22/05/15.
 */
public class MailBox {

    public List<Mail> getMails() {
        return mails;
    }

    public void setMails(List<Mail> mails) {
        this.mails = mails;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    private String student;
    private List<Mail> mails = new ArrayList<>();

    public Mail Rechercher(String str)
    {
        Mail mail = new Mail("","","");
        try
        {
            List<Mail> mails = getMails();
            for(int i = 0 ; i < mails.size() ; i++)
            {
                if(str == mails.get(i).getFrom() ||
                        str == mails.get(i).getTo() ||
                        str == mails.get(i).getBody())

                {
                    mail = mails.get(i);
                    break;
                }
            }
        }
        catch (NullPointerException e)
        {
            throw (e);
        }
        return mail;
    }
}
