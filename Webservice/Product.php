<?php
require_once 'DTBManager.php';

class Product {
    
    private $id;
    private $reference;
    private $name;
    private $price;
    private $number;
    private $imgSrc;
    private $used;
    
    public function __construct($id, $reference, $name, $price, $src, $number = 1) {
        $this->id = $id;
        $this->reference = $reference;
        $this->name = $name;
        $this->price = $price;
        $this->number = $number;
        $this->imgSrc = $src;
        $this->used = false;
    }
    function getImgSrc() {
        return $this->imgSrc;
    }

    function setImgSrc($imgSrc) {
        $this->imgSrc = $imgSrc;
    }

    public function getId() {
        return $this->id;
    }
    
    public function getReference() {
        return $this->reference;
    }

    public function getName() {
        return $this->name;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getNumber() {
        return $this->number;
    }
    
    public function getUsed() {
        return $this->used;
    }
    
    public function setReference($reference) {
        $this->reference = $reference;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setPrice($price) {
        $this->price = $price;
    }
    
    public function setNumber($number) {
        $this->number = $number;
    }
    
    public function useProduct() {
        $this->used = true;
        
    }
    
    public function getUsedForUser($id_user){
        $connection = (new DTBManager("pierrema101991"))->getConnection();
        $query = "SELECT used FROM Link_product_user WHERE id_product ='".$this->id."' AND id_user='".$id_user."'";
        $result = $connection->query($query);
        $result = $result->fetch_array();
        $this->used = $result['used'];
        return $result['used'];
    }
    
    public static function getProductByReference($ref){
        $connection = (new DTBManager("pierrema101991"))->getConnection();
        $query = "SELECT * FROM Produits WHERE reference ='".$ref."'";
        $result = $connection->query($query);
        $result = $result->fetch_array();
        if($result){
            return new Product($result['id'], $result['reference'], $result['name'], $result['price'], $result['src']);
        }
        else{
            return false;
        }
        
    }
    
    public static function getProductById($id){
        $connection = (new DTBManager("pierrema101991"))->getConnection();
        $query = "SELECT * FROM Produits WHERE id ='".$id."'";
        if($result = $connection->query($query)){
            $result = $result->fetch_array();
            return new Product($result['id'], $result['reference'], $result['name'], $result['price'], $result['src']);
        }
        else{
            return false;
        }
        
    }
    
    
    
}
