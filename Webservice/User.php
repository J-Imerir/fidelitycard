<?php
require_once 'DTBManager.php';
require_once 'Product.php';

define('PERCENT_REDUC', 0.10);

class User {
    
    private $id;
    private $username;
    private $password;
    private $productList;
    private $reductionHistory = array();
    private $reduction;
    
    public function __construct($id, $username, $password) {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->productList = array();
        
        $this->getAllProducts();
        $this->getReductionHistory();
        $this->calculReduction();
    }

    public function getId() {
        return $this->id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getProductList() {
        return $this->productList;
    }
    
    public function getReduction() {
        return $this->reduction;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setProductList($productList) {
        $this->productList = $productList;
    }
    
    private function getReductionHistory(){
        $connection = (new DTBManager("pierrema101991"))->getConnection();
        $query = "SELECT amount FROM Reduc_history WHERE user_id =".$this->id;
        $result = $connection->query($query);
        while($row = $result->fetch_assoc()){
            array_push($this->reductionHistory, $row['amount']);
        }
        
    }
    
    public function addProductToList($product){
        
        $connection = (new DTBManager("pierrema101991"))->getConnection();
        if(!($linkId = $this->productIsInList($product))){
            $query = "INSERT INTO Link_product_user VALUES ('', ".$product->getId().", ".$this->id.", ".$product->getNumber().", 0)";
            $result = $connection->query($query);
            if($result){
                array_push($this->productList, $product);
                return true;
            }
            else{
                return false;
            }
        }
        else{
            $query = "UPDATE Link_product_user SET nb_product = (nb_product + 1) WHERE id_product=".$linkId." AND id_user=".$this->id;
            $result = $connection->query($query);
            if($result){
                foreach($this->productList as $key => $productL){
                    if($productL->getId() == $product->getId()){
                        $productL->setNumber($productL->getNumber() + 1);
                        $this->productList[$key] = $productL;
                        break;
                    }
                }
                return true;
            }
            else{
                return false;
            }
        }
    }
    
    public function productIsInList($product){
        foreach($this->productList as $key => $productL){
            if($productL->getId() == $product->getId()){
                return $productL->getId();
            }
        }
        return 0;
    }
    
    public function authenticate($inputPassword){
        if(md5($inputPassword) == $this->password){
            return true;
        }
        else{
            return false;
        }
    }
    
    private function calculReduction(){
        $reduction = 0;
        foreach ($this->productList as $product){
            $reduction += ($product->getPrice() * $product->getNumber() * PERCENT_REDUC);
        }
        foreach ($this->reductionHistory as $history){
            $reduction -= $history;
        }
        
        $this->reduction =round( $reduction , 2 );
    }
    
    public function getOldReduction(){
        $oldReduction = 0;
        foreach ($this->reductionHistory as $history){
            $oldReduction += $history;
        }
        return $oldReduction;
    }
    
    public function useReduction(){
        $connection = (new DTBManager("pierrema101991"))->getConnection();
        $this->calculReduction();
        $query = "INSERT INTO Reduc_history VALUES ('', ".$this->id.", ".$this->reduction.")";
        $result = $connection->query($query);
        if($result){
            $this->reduction = 0;
            return true;
        }
        else{
            return false;
        }
    }

    public static function getUserByUsername($username){
        $connection = (new DTBManager("pierrema101991"))->getConnection();
        $query = "SELECT * FROM Users WHERE username ='".$username."'";
        $result = $connection->query($query);
        if($result =  $result->fetch_array() ){
            return new User($result['id'], $result['username'], $result['password']);
        }
        else{
            return false;
        }
    }
    
    public static function getUserById($id){
        $connection = (new DTBManager("pierrema101991"))->getConnection();
        $query = "SELECT * FROM Users WHERE id ='".$id."'";
        $result = $connection->query($query);
        if($result =  $result->fetch_array() ){
            return new User($result['id'], $result['username'], $result['password']);
        }
        else{
            return false;
        }
    }
    
    public static function userExist($username){
        if(self::getUserByUsername($username)){
            return true;
        }

        return false;
    }
    
    public static function createUser($username, $password){
        if(self::userExist($username)){
            return false;
        }
        $connection = (new DTBManager("pierrema101991"))->getConnection();
        $query = "INSERT INTO Users VALUES ('', '".$username."', '".md5($password)."')";
        $result = $connection->query($query);
        if($result){
            $user = new User($connection->insert_id, $username, md5($password));
            return $user;
        }
        else{
            return false;
        }
    }
    
    private function getAllProducts(){
        $connection = (new DTBManager("pierrema101991"))->getConnection();
        $query = "SELECT id_product, nb_product FROM Link_product_user WHERE id_user ='".$this->id."'";
        if($result = $connection->query($query)){
            while($row = $result->fetch_assoc()){
                $product = Product::getProductById($row['id_product']);
                $product->setNumber($row['nb_product']);
                $product->getUsedForUser($this->id);
                array_push($this->productList, $product);
            }
            return true;
        }
        else{
            return false;
        }
    }
    
}
