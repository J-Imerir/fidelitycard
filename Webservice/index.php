<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once 'User.php';
header('Content-Type: application/json');


if(isset($_GET['get']) && isset($_GET['username']) && isset($_GET['inputPassword'])){
    $username = $_GET['username'];
    $inputPassword = $_GET['inputPassword'];
    
   if($user = User::getUserByUsername($username)){
        if(!$user->authenticate($inputPassword)){
            echo '{ "info" : "password is false" }';
        }
        else{
             $json = '{';
             $json.='"info" : "success",';
             $json.='"id" : '.$user->getId().',';
             $json.='"username" : "'.$user->getUsername().'",';
             $json.='"reduction" : '.$user->getReduction().',';
             $json.='"oldReduction" : '.$user->getOldReduction().',';
             $json.='"products" : [';
                foreach($user->getProductList() as $key => $product){
                    $json.= '{';
                    $json.= '"id" : '.$product->getId().',';
                    $json.= '"reference" : "'.$product->getReference().'",';
                    $json.= '"name" : "'.$product->getName().'",';
                    $json.= '"price" : '.$product->getPrice().',';
                    $json.= '"src" : "'.$product->getImgSrc().'",';
                    $json.= '"number" : '.$product->getNumber();
                    $json.= '}';
                    if($key != count($user->getProductList())-1){
                       $json.= ',';
                    }
                }
             $json.=']';
             $json.='}';
             
             echo $json;
        }
   }
   else{
       echo '{ "info" : "no user" }';
   }   
    
}
else if(isset($_GET['set']) && isset($_GET['username']) && isset($_GET['inputPassword'])){
    
    $username = $_GET['username'];
    $inputPassword = $_GET['inputPassword'];
    
    if(!User::userExist($username)){
        if($user = User::createUser($username, $inputPassword)){
            $json = '{';
            $json .= '"info" : "success",';
            $json .= '"id" : "'.$user->getId().'",';
            $json .= '"username" : "'.$user->getUsername().'",';
            $json .= '"password" : "'.$user->getPassword().'"';
            $json .= '}';
            echo $json;
        }
        else{
            echo '{ "info" : "error at creation" }';
        }
    }
    else{
        echo '{ "info" : "name already used" } ';
    }
}
else if(isset($_GET['addProduct']) && isset($_GET['userId']) && isset($_GET['productRef'])){

        $userid = $_GET['userId'];
        $productRef = $_GET['productRef'];
    
        $product = Product::getProductByReference($productRef);
        $user = User::getUserById($userid);
        
        if(($user) && ($product)){

            if($user->addProductToList($product)){
                $json = '{ "info" : "success",';
                $json.='"products" : [';
                foreach($user->getProductList() as $key => $product){
                    $json.= '{';
                    $json.= '"id" : '.$product->getId().',';
                    $json.= '"reference" : "'.$product->getReference().'",';
                    $json.= '"name" : "'.$product->getName().'",';
                    $json.= '"price" : '.$product->getPrice().',';
                    $json.= '"src" : "'.$product->getImgSrc().'",';
                    $json.= '"number" : '.$product->getNumber();
                    $json.= '}';
                    if($key != count($user->getProductList())-1){
                       $json.= ',';
                    }
                }
             $json.=']}';
            echo $json;
            
            }
            else{
                echo '{ "info" : "failure" } ';
            }
        }
        else{
            echo '{ "info" : "wrong parameters" } ';
        }
    
}
else if (isset($_GET['useReduc']) && isset($_GET['userId'])){
    $userid = $_GET['userId'];
    $user = User::getUserById($userid);
    if($user){
        if($user->useReduction()){
            echo '{ "info" : "success" } ';
        }
        else{
            echo '{ "info" : "failure" } ';
        }
    }
    else{
        echo '{"info" : "wrong parameters"}';
    }
}


function devAff($a){
    if(gettype($a) == 'array' || gettype($a) == 'object'){
        echo '<pre>';
        print_r($a);
        echo '</pre>';
    }
    else{
        echo $a;
    }
}
?>

