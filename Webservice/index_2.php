<?php
require_once 'flight/Flight.php';
require_once 'User.php';
header('Content-Type: application/json');

Flight::route('/test', function(){
    
    echo '{"info" : "coucou"}';
   
});

Flight::route('/get/@username/@inputPassword', function($username, $inputPassword){
   if($user = User::getUserByUsername($username)){
        if(!$user->authenticate($inputPassword)){
            echo '{ "info" : "password is false" }';
        }
        else{
             $json = '{';
             $json.='"id" : '.$user->getId().',';
             $json.='"username" : "'.$user->getUsername().'",';
             $json.='"reduction" : "'.$user->getReduction().'",';
             $json.='"products" : [';
                 foreach($user->getProductList() as $key => $product){
                     $json.= '{';
                     $json.= '"id" : '.$product->getId().',';
                     $json.= '"reference" : "'.$product->getReference().'",';
                     $json.= '"name" : "'.$product->getName().'",';
                     $json.= '"price" : '.$product->getPrice().',';
                     $json.= '"number" : '.$product->getNumber();
                     $json.= '}';
                     if($key != count($user->getProductList())-1){
                        $json.= ',';
                     }
                 }
             $json.=']';
             $json.='}';
             
             echo $json;
        }
   }
   else{
       echo '{ "info" : "no user" }';
   }   
    
});

Flight::route('/set/@username/@inputPassword', function($username, $inputPassword){
    
    if(!User::userExist($username)){
        if($user = User::createUser($username, $inputPassword)){
            $json = '{';
            $json .= '"id" : "'.$user->getId().'",';
            $json .= '"username" : "'.$user->getUsername().'",';
            $json .= '"password" : "'.$user->getPassword().'"';
            $json .= '}';
            echo $json;
        }
        else{
            echo '{ "info" : "error at creation" }';
        }
    }
    else{
        echo '{ "info" : "name already used" } ';
    }
});

Flight::route('/addProduct/@userid/@productRef', function($userid, $productRef){
        $product = Product::getProductByReference($productRef);
        $user = User::getUserById($userid);
        
        if(($user) && ($product)){

            if($user->addProductToList($product)){
                echo '{ "info" : "success" } ';
            }
            else{
                echo '{ "info" : "failure" } ';
            }
        }
        else{
            echo '{ "info" : "wrong parameters" } ';
        }
    
});

Flight::start();

function devAff($a){
    if(gettype($a) == 'array' || gettype($a) == 'object'){
        echo '<pre>';
        print_r($a);
        echo '</pre>';
    }
    else{
        echo $a;
    }
}
?>

